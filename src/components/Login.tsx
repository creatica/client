import * as React from 'react';
import { Container, Form, FormGroup, Button, Input, Label, Alert, Row, Col } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Axios from 'axios';

export type LoginComponentProps = {}
export type LoginComponentState = {
  email: string
  password: string
  alertMessage: string
  redirect: boolean
}

export class LoginComponent extends React.Component<LoginComponentProps, LoginComponentState> {
  constructor(props: LoginComponentProps) {
    super(props)

    this.state = {
      email: '',
      password: '',
      alertMessage: '',
      redirect: false
    }
  }

  onEmailChange: (event: React.ChangeEvent<HTMLInputElement>) => void = e => {
    const email = e.target.value;
    this.setState({ email });
  }

  onPassChange: (event: React.ChangeEvent<HTMLInputElement>) => void = e => {
    const password = e.target.value;
    this.setState({ password });
  }

  canSubmit: () => boolean = () => {
    if (this.state.email && this.state.password) {
      return true;
    }
    return false;
  }

  submit: () => void = () => {
    if (this.state.email.includes("@hr.nl")) {
      if (this.state.password) {
        Axios.post('http://localhost:5000/user/login', {
          email: this.state.email,
          password: this.state.password
        }).then(res => {
          console.log('result: ', res)
          localStorage.setItem('userId', res.data.ID);
          this.setState({ redirect: true })
        }).catch(err => {
          console.log(err)
          const alertMessage = "Verkeerde email - wachtwoord combinatie, probeer het nogmaals.";
          this.setState({ alertMessage });
        })
        
      }
      else {
        const alertMessage = "Vul je wachtwoord in";
        this.setState({ alertMessage });
      }
    }
    else {
      const alertMessage = "Vul een correct hr-email in";
      this.setState({ alertMessage });
    }
  }


  render() {
    if(this.state.redirect) {
      return <Redirect to='/filter' />
    }
    return (
      <Container className="mx">
        <h1 className="text-center test">Log in Roomy</h1>
        <p>Login met je HR-mail om een lokaal te vinden</p>
        <Form onSubmit={this.submit}>
          <FormGroup>
            <Label>HR-mail:</Label>
            <Input className="mb-2" type="text" onChange={this.onEmailChange} />

            <Label>Wachtwoord:</Label>
            <Input className="mb-2" type="password" onChange={this.onPassChange} />
          </FormGroup>

          <FormGroup>
            {this.state.alertMessage ? <Alert color="danger">{this.state.alertMessage}</Alert> : ''}
          </FormGroup>
          <FormGroup>
            <Row>
              <Col>
                <Button className="bg-primary" disabled={!this.canSubmit()} onClick={this.submit}>Log in</Button>
              </Col>
              <Col>
                <Link to="/register">
                  <Button>Registreren</Button>
                </Link>
              </Col>
            </Row>
          </FormGroup>
        </Form>
      </Container>
    )
  }
}
