import * as React from 'react'
import {
  Container,
  Form,
  FormGroup,
  Button,
  Input,
  Label,
  Alert,
  Row,
  Col
} from 'reactstrap'
import { Link, Redirect } from 'react-router-dom'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLongArrowAltLeft } from '@fortawesome/free-solid-svg-icons'

export type OverviewProps = {}
export type OverviewState = {
  present: number[]
  building: string
  floor: number
}

export class OverviewComponent extends React.Component<
  OverviewProps,
  OverviewState
> {
  constructor(props: OverviewProps) {
    super(props)

    this.state = {
      present: [0, 5, 10, 3, 2, 8, 16, 4, 30, 24, 45, 7],
      building: 'H',
      floor: 1
    }

  }

  getColor: (present: number) => string = present => {
    if (present > 15) {
      return 'red'
    } else if (present > 8) {
      return 'orange'
    }

    return 'green'
  }

  componentWillMount: () => void = () => {
    const interval = setInterval(() => {
      this.retrievePresent(false)
    }, 8000)
  }

  retrievePresent: (user: boolean) => void = (user) => {
    const present: number[] = this.state.present;

    axios.get('http://192.168.137.142:80/').then(res => {
      console.log(res);
      present[0] = parseInt(res.data.persons);
    })

    for (let i = 1; i < this.state.present.length; i++) {
      if(user){
        present[i] = this.generatePresent()
      }else if(Math.floor(Math.random() * 10) >= 7) {
        present[i] = this.generatePresent()
      }
    }

    this.setState({ present })
  }

  generatePresent: () => number = () => {
    return Math.floor(Math.random() * 30)
  }

  onBuildingChange: (
    event: React.ChangeEvent<HTMLSelectElement>
  ) => void = e => {
    const building = e.target.value
    this.retrievePresent(true)
    this.setState({ building })
  }

  onFloorChange: (event: React.ChangeEvent<HTMLSelectElement>) => void = e => {
    const floor = e.target.value
    this.retrievePresent(true)
    this.setState({ floor: parseInt(floor) })
  }

  render() {
    return (
      <Container>
        <h1>
          <Link to="/filter">
            <Button className="bg-danger mr-2">
              <FontAwesomeIcon size="1x" icon={faLongArrowAltLeft} />
            </Button>
          </Link>
          Overzicht:
        </h1>
        <Form>
          <Row>
            <Col>
              <Label className="mr-2">Gebouw:</Label>
              <br />
              <select
                value={this.state.building}
                onChange={this.onBuildingChange}
              >
                <option value="H">H</option>
                <option value="WD">WD</option>
                <option value="WN">WN</option>
              </select>
            </Col>
            <Col>
              <Label className="mr-2">Verdieping:</Label>
              <br />
              <select value={this.state.floor} onChange={this.onFloorChange}>
                <option value={1}>1</option>
                <option value={2}>2</option>
                <option value={3}>3</option>
                <option value={4}>4</option>
                <option value={5}>5</option>
              </select>
            </Col>
          </Row>
        </Form>
        <Row className="mt-4">
          <Col
            className="border-bottom border-white p-2 text-white"
            style={{
              backgroundColor: this.getColor(this.state.present[0]),
              height: '120px',
            }}
          >
            <strong><h5>Lokaal: {this.state.building}.{this.state.floor}.312</h5></strong>
            <hr />
            <strong><h6>Aanwezig: {this.state.present[0]}</h6></strong>
          </Col>
          <Col
            className="border-bottom border-left border-right border-white p-2"
            style={{
              backgroundColor: this.getColor(this.state.present[1]),
              height: '120px'
            }}
          >
            Lokaal: {this.state.building}.{this.state.floor}.313
            <hr />
            Aanwezig: {this.state.present[1]}
          </Col>
          <Col
            className="border-bottom border-white p-2"
            style={{
              backgroundColor: this.getColor(this.state.present[2]),
              height: '120px'
            }}
          >
            Lokaal: {this.state.building}.{this.state.floor}.314
            <hr />
            Aanwezig: {this.state.present[2]}
          </Col>
        </Row>
        <Row className="mt-0">
          <Col
            className="border-bottom border-white p-2"
            style={{
              backgroundColor: this.getColor(this.state.present[3]),
              height: '120px'
            }}
          >
            Lokaal: {this.state.building}.{this.state.floor}.315
            <hr />
            Aanwezig: {this.state.present[3]}
          </Col>
          <Col
            className="border-bottom border-left border-right border-white p-2"
            style={{
              backgroundColor: this.getColor(this.state.present[4]),
              height: '120px'
            }}
          >
            Lokaal: {this.state.building}.{this.state.floor}.316
            <hr />
            Aanwezig: {this.state.present[4]}
          </Col>
          <Col
            className="border-bottom border-white p-2"
            style={{
              backgroundColor: this.getColor(this.state.present[5]),
              height: '120px'
            }}
          >
            Lokaal: {this.state.building}.{this.state.floor}.317
            <hr />
            Aanwezig: {this.state.present[5]}
          </Col>
        </Row>
        <Row className="mt-0">
          <Col
            className="border-bottom border-white p-2"
            style={{
              backgroundColor: this.getColor(this.state.present[6]),
              height: '120px'
            }}
          >
            Lokaal: {this.state.building}.{this.state.floor}.318
            <hr />
            Aanwezig: {this.state.present[6]}
          </Col>
          <Col
            className="border-bottom border-left border-right border-white p-2"
            style={{
              backgroundColor: this.getColor(this.state.present[7]),
              height: '120px'
            }}
          >
            Lokaal: {this.state.building}.{this.state.floor}.320
            <hr />
            Aanwezig: {this.state.present[7]}
          </Col>
          <Col
            className="border-bottom border-white p-2"
            style={{
              backgroundColor: this.getColor(this.state.present[8]),
              height: '120px'
            }}
          >
            Lokaal: {this.state.building}.{this.state.floor}.321
            <hr />
            Aanwezig: {this.state.present[8]}
          </Col>
        </Row>
        <Row className="mt-0">
          <Col
            className="border-bottom border-white p-2"
            style={{
              backgroundColor: this.getColor(this.state.present[9]),
              height: '120px'
            }}
          >
            Lokaal: {this.state.building}.{this.state.floor}.322
            <hr />
            Aanwezig: {this.state.present[9]}
          </Col>
          <Col
            className="border-bottom border-left border-right border-white p-2"
            style={{
              backgroundColor: this.getColor(this.state.present[10]),
              height: '120px'
            }}
          >
            Lokaal: {this.state.building}.{this.state.floor}.324
            <hr />
            Aanwezig: {this.state.present[10]}
          </Col>
          <Col
            className="border-bottom border-white p-2"
            style={{
              backgroundColor: this.getColor(this.state.present[11]),
              height: '120px'
            }}
          >
            Lokaal: {this.state.building}.{this.state.floor}.328
            <hr />
            Aanwezig: {this.state.present[11]}
          </Col>
        </Row>
      </Container>
    )
  }
}
