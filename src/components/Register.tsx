import * as React from 'react'
import {
  Container,
  Form,
  FormGroup,
  Button,
  Input,
  Label,
  Alert,
  Row,
  Col
} from 'reactstrap'
import { Link, Redirect } from 'react-router-dom'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css'

export type RegisterComponentProps = {}
export type RegisterComponentState = {
  name: string
  email: string
  password: string
  password1: string
  alertMessage: string
  type: string
  redirect: boolean
}

export class RegisterComponent extends React.Component<
  RegisterComponentProps,
  RegisterComponentState
> {
  constructor(props: RegisterComponentProps) {
    super(props)

    this.state = {
      name: '',
      email: '',
      password: '',
      password1: '',
      alertMessage: '',
      type: 'student',
      redirect: false
    }
  }

  onNameChange: (event: React.ChangeEvent<HTMLInputElement>) => void = e => {
    const name = e.target.value
    this.setState({ name })
  }

  onEmailChange: (event: React.ChangeEvent<HTMLInputElement>) => void = e => {
    const email = e.target.value
    this.setState({ email })
  }

  onTypeChange: (event: React.ChangeEvent<HTMLSelectElement>) => void = e => {
    const type = e.target.value
    this.setState({ type })
  }

  onPassChange: (event: React.ChangeEvent<HTMLInputElement>) => void = e => {
    const password = e.target.value
    this.setState({ password })
  }

  onPass1Change: (event: React.ChangeEvent<HTMLInputElement>) => void = e => {
    const password1 = e.target.value
    this.setState({ password1 })
  }

  canSubmit: () => boolean = () => {
    if (
      this.state.email &&
      this.state.password &&
      this.state.password1 &&
      this.state.name
    ) {
      return true
    }
    return false
  }

  submit: () => void = () => {
    if (this.state.name) {
      if (this.state.email.includes('@hr.nl')) {
        if (
          this.state.password &&
          this.state.password === this.state.password1
        ) {
          const id = this.state.email.replace('@hr.nl', '')
          const email = this.state.email
          const pass = this.state.password
          const name = this.state.name
          const type = this.state.type

          axios
            .post('http://localhost:5000/user', {
              ID: id,
              type: type,
              name: name,
              email: email,
              password: pass
            })
            .then(res => {
              this.setState({ redirect: true })              
            })
            .catch(err => {
              console.log(err)
              const alertMessage = 'Er bestaat al een account met dit email adres, probeer in te loggen.'
              this.setState({ alertMessage })
            
            })
        } else {
          const alertMessage = 'Je wachtwoorden komen niet overeen'
          this.setState({ alertMessage })
        }
      } else {
        const alertMessage = 'Vul een correct hr-email in'
        this.setState({ alertMessage })
      }
    } else {
      const alertMessage = 'Vul je naam in'
      this.setState({ alertMessage })
    }
  }

  render() {
    if(this.state.redirect) {
      return <Redirect to="/" />
    }
    console.log(this.state)
    return (
      <Container className="mx">
        <h1 className="text-center">Registreren Roomy</h1>
        <Form onSubmit={this.submit}>
          <FormGroup>
            <label>Volledige naam:</label>
            <Input type="text" onChange={this.onNameChange} />

            <Label>HR-mail:</Label>
            <Input className="mb-2" type="text" onChange={this.onEmailChange} />

            <Label>Student of docent</Label>
            <br />
            <select value={this.state.type} onChange={this.onTypeChange}>
              <option value="student">Student</option>
              <option value="docent">Docent</option>
            </select>
            <br />
            <br />
            <Label>Kies een wachtwoord:</Label>
            <Input
              className="mb-2"
              type="password"
              onChange={this.onPassChange}
            />
            <Input
              className="mb-2"
              type="password"
              onChange={this.onPass1Change}
            />
          </FormGroup>

          <FormGroup>
            {this.state.alertMessage ? (
              <Alert color="danger">{this.state.alertMessage}</Alert>
            ) : (
              ''
            )}
          </FormGroup>

          <FormGroup>
            <Row>
              <Col>
                <Button className="bg-primary" disabled={!this.canSubmit()} onClick={this.submit}>
                  Registreren
                </Button>
              </Col>
              <Col>
                <Link to="/">
                  <Button>Log in</Button>
                </Link>
              </Col>
            </Row>
          </FormGroup>
        </Form>
      </Container>
    )
  }
}
