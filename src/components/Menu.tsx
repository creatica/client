import * as React from 'react';
import { Container, Form, FormGroup, Button, Input, Label, Alert, Row, Col } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faCalendarAlt, faCamera } from '@fortawesome/free-solid-svg-icons'

export type MenuComponentProps = {}
export type MenuComponentState = {}

export class MenuComponent extends React.Component<MenuComponentProps, MenuComponentState> {
  constructor(props: MenuComponentProps) {
    super(props)

    this.state = {
    }
  }

  render() {
    return (
      <Row className="text-blue p-1">
        <Col>
          <Link className="text-blue" to="/filter">
            <Row className="ml-4">
              <FontAwesomeIcon size="2x" icon={faSearch} />
            </Row>
            <Row className="ml-3">
              Zoeken
          </Row>
          </Link>
        </Col>
        <Col className="border-left border-right border-blue">
          <Link className="text-blue" to="/reservation">
            <Row className="ml-4">
              <FontAwesomeIcon size="2x" icon={faCalendarAlt} />
            </Row>
            <Row className="ml-1">
              Reserveren
          </Row>
          </Link>
        </Col>
        <Col>
          <Link className="text-blue" to="/scan">
            <Row className="ml-4">
              <FontAwesomeIcon size="2x" icon={faCamera} />
            </Row>
            <Row className="ml-3">
              Scannen
          </Row>
          </Link>
        </Col>
      </Row>
    )
  }
}
