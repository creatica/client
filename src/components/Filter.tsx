import * as React from 'react'
import {
  Container,
  Form,
  FormGroup,
  Button,
  Input,
  Label,
  Alert,
  Row,
  Col
} from 'reactstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Link, Redirect } from 'react-router-dom'
import { MenuComponent } from './Menu'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faWindowRestore,
  faLanguage,
  faArrowDown,
  faInfoCircle
} from '@fortawesome/free-solid-svg-icons'
import { User } from '../model'
import { number } from 'prop-types'
import Axios from 'axios';

type FilterFormState =
  | {
      capacity?: number
      building?: 'H' | 'WD' | 'WN'
      floor?: number
      type: 'project' | 'stilte'
    }
  | {
      type: 'prive'
      capacity?: number
      building?: string
      floor?: number
      duration: number
    }

export type FilterComponentProps = {}
export type FilterComponentState = {
  redirect: string,
  showPreferences: boolean
} & FilterFormState

export class FilterComponent extends React.Component<
  FilterComponentProps,
  FilterComponentState
> {
  constructor(props: FilterComponentProps) {
    super(props)

    this.state = {
      redirect: '',
      capacity: 1,
      building: 'H',
      floor: 0,
      type: 'project',
      showPreferences: true
    }
  }

  getUser: () => User = () => {
    return new User() //Uit windows get item halon
  }

  onBuildingChange: (
    event: React.ChangeEvent<HTMLSelectElement>
  ) => void = e => {
    const building = e.target.value
    this.setState({ building })
  }

  onFloorChange: (event: React.ChangeEvent<HTMLSelectElement>) => void = e => {
    const floor = e.target.value
    this.setState({ floor: parseInt(floor) })
  }

  onTypeChange: (event: React.ChangeEvent<HTMLSelectElement>) => void = e => {
    const type = e.target.value
    this.setState({ type: type as 'stilte' | 'project' | 'prive' })
  }

  onCapacityChange: (
    event: React.ChangeEvent<HTMLInputElement>
  ) => void = e => {
    const capacity = e.target.value
    this.setState({ capacity: parseInt(capacity) })
  }

  togglePreferences: () => void = () => {
    let showPreferences = this.state.showPreferences
    this.setState({ showPreferences: !showPreferences })
  }

  getRoom: () => void = () => {
    //axios.post de filter data naar zn account .then( set state redirect suggestions)
    const userID: string = localStorage.getItem('userId');

    console.log('ID: ', userID);

    Axios.post('http://localhost:5000/classroom/filter', {
      amount: this.state.capacity,
      building: this.state.building,
      floor: this.state.floor,
      type: this.state.type,
    }, { headers: {
      postID: userID
    }}).then(res => {
      console.log('filter post:', res);
      this.setState({ redirect: 'suggestions'});
    }).catch(err => {
      console.log('Error: ', err);
    })
  }

  selfService: () => void = () => {
    this.setState({ redirect: 'overview'});
  }

  render() {
    console.log(this.state);
    if(this.state.redirect) {
      return <Redirect to={`/${this.state.redirect}`} />
    }
    
    return (
      <Container className="mx">
        <Row style={{ flexDirection: 'column', height: '100vh' }}>
          <Col>
            <h2>Roomy lokaal zoeken</h2>
            <p className="mt-2">
              Vul je voorkeuren hieronder in om een toepasselijk lokaal te
              vinden
            </p>
            <Container className="bg-info mb-0 pb-0">
              <div
                className="border-bottom border-white"
                onClick={this.togglePreferences}
              >
                <h2 className="text-left text-white">
                  Voorkeuren <FontAwesomeIcon size="1x" icon={faArrowDown} />
                </h2>
              </div>
              {this.state.showPreferences ? (
                <Form className="mt-1">
                  <FormGroup>
                    <strong>Locatie:</strong>
                    <Row>
                      <Col>
                        <Label className="mr-2">Gebouw:</Label>
                        <br />
                        <select
                          value={this.state.building}
                          onChange={this.onBuildingChange}
                        >
                          <option value="H">H</option>
                          <option value="WD">WD</option>
                          <option value="WN">WN</option>
                        </select>
                      </Col>
                      <Col>
                        <Label className="mr-2">Verdieping:</Label>
                        <br />
                        <select
                          value={this.state.floor}
                          onChange={this.onFloorChange}
                        >
                          <option value={1}>1</option>
                          <option value={2}>2</option>
                          <option value={3}>3</option>
                          <option value={4}>4</option>
                          <option value={5}>5</option>
                        </select>
                      </Col>
                    </Row>
                    <hr />
                    <Row>
                      <Col>
                        <Label>Activiteit:</Label>
                        <br />
                        <select
                          className="w-75"
                          value={this.state.type}
                          onChange={this.onTypeChange}
                        >
                          <option value="project">Project</option>
                          <option value="stilte">Stilte</option>
                          <option value="prive">Privé</option>
                        </select>
                      </Col>
                      <Col>
                        <Label>Aantal mensen</Label>
                        <Input
                          className="w-50"
                          type="number"
                          value={this.state.capacity}
                          onChange={this.onCapacityChange}
                        />
                      </Col>
                    </Row>
                  </FormGroup>
                </Form>
              ) : (
                ''
              )}
              <Row className="p-2">
                {this.state.capacity > 50 && this.state.showPreferences ? (
                  <Alert color="danger">
                    Met{' '}
                    {this.state.capacity.toString().length > 3
                      ? ' zoveel mensen '
                      : this.state.capacity}{' '}
                    mensen inplannen vereist roostermaker rechten.
                  </Alert>
                ) : (
                  ''
                )}
              </Row>
            </Container>
            <Row>
              {!this.state.showPreferences ? (
                <Alert className="mt-5 text-danger" color="bg-info">
                  <FontAwesomeIcon size="1x" icon={faInfoCircle} />
                  Als je geen preferences invult krijg je een willekeurig lokaal
                  aangewezen.
                </Alert>
              ) : (
                ''
              )}
            </Row>
          </Col>
          <Col style={{ alignSelf: 'flex-end' }}>
            <Row
              className="menu-wrapper"
              style={{ height: '100%', alignItems: 'flex-end' }}
            >
              <Col className="menu-inner">
                <Button
                  className="bg-primary w-100 mb-1"
                  onClick={this.getRoom}
                >
                  Zoek lokaal
                </Button>
                <Button
                  className="bg-success w-100 mb-2 mt-1"
                  onClick={this.selfService}
                >
                  Zelf selecteren
                </Button>
                <MenuComponent />
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    )
  }
}
