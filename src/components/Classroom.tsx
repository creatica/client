import * as React from 'react'
import {
  Container,
  Form,
  FormGroup,
  Button,
  Input,
  Label,
  Alert,
  Row,
  Col
} from 'reactstrap'
import { Link, Redirect } from 'react-router-dom'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Classroom } from '../model'

export type ClassroomProps = Classroom & {
  key: string,
  time: string,
  present: number
}

export const ClassroomComponent: React.SFC<ClassroomProps> = props => {
  
  console.log(props.key);
  console.log(props);
  return (
    <Link to={`/classroom/:${props.ID}`}>
      <Row className="border-bottom border-white">
        <Button className="color-primary p-2 w-100">
          <Row className="m-2">
            <h3>{props.ID}</h3>
          </Row>
          <Row>
            <Col>
              Vrij tot {props.time}
            </Col>
            <Col>
              {props.present} {props.present == 1 ? 'persoon' : 'mensen'} aanwezig
            </Col>
          </Row>
        </Button>
      </Row>
    </Link>
  )
}
