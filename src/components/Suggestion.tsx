import * as React from 'react'
import {
  Container,
  Form,
  FormGroup,
  Button,
  Input,
  Label,
  Alert,
  Row,
  Col,
  Table
} from 'reactstrap'
import { Link, Redirect } from 'react-router-dom'
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Classroom } from '../model'
import { ClassroomComponent } from './Classroom'
import { MenuComponent } from './Menu'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLongArrowAltLeft } from '@fortawesome/free-solid-svg-icons'

export type SuggestionComponentProps = {}
export type SuggestionComponentState = {
  suggestions: Classroom[],
  redirect: boolean,
}

export class SuggestionComponent extends React.Component<
  SuggestionComponentProps,
  SuggestionComponentState
> {
  constructor(props: SuggestionComponentProps) {
    super(props)

    this.state = {
      suggestions: [],
      redirect: false
    }
  }

  toOverview: () => void = () => {
    this.setState({redirect: true})
  }

  retrieveSuggestions: () => Classroom[] = () => {
    const klas1 = new Classroom()
    klas1.ID = 'H.1.312'
    klas1.building = 'H'
    klas1.floor = '1'
    klas1.peoplePresent = 0

    const klas2 = new Classroom()
    klas2.ID = 'H.4.315'
    klas2.building = 'H'
    klas2.floor = '4'
    klas2.peoplePresent = 0

    const klas3 = new Classroom()
    klas3.ID = 'WD.3.001'
    klas3.building = 'WD'
    klas3.floor = '3'
    klas3.peoplePresent = 0

    const suggestions: Classroom[] = [
      {
        ID: 'H.1.312',
        capacity: 10,
        peoplePresent: 6,
        building: 'H',
        floor: '1',
        type: 'project',
        reservations: []
      },
      {
        ID: 'WD.3.069',
        capacity: 30,
        peoplePresent: 6,
        building: 'WD',
        floor: '3',
        type: 'stilte',
        reservations: []
      },
      {
        ID: 'WN.5.041',
        capacity: 30,
        peoplePresent: 6,
        building: 'WD',
        floor: '3',
        type: 'project',
        reservations: []
      },
      {
        ID: 'H.3.420',
        capacity: 30,
        peoplePresent: 6,
        building: 'WD',
        floor: '3',
        type: 'project',
        reservations: []
      }
    ]

    return suggestions
  }

  render() {
    if(this.state.redirect) {
      return <Redirect to="/overview" />
    }
    const tijden: string[] = ['15:30', '16:00', '17:00', '15:35', '16:23']
    const mensen: number[] = [2, 0, 5, 1, 8, 1, 12, 34]
    return (
      <Container className="mx">
        <Row style={{ flexDirection: 'column', height: '100vh' }}>
          <Col style={{ alignSelf: 'flex-end' }}>
            <h1>
              <Link to='/filter'>
                <Button className='bg-danger mr-2'>
                  <FontAwesomeIcon size="1x" icon={faLongArrowAltLeft} />
                </Button>
              </Link>
              Suggesties:
            </h1>

            {this.retrieveSuggestions().map((classroom, i) => {
              return (
                <ClassroomComponent
                  {...classroom}
                  time={tijden[i]}
                  present={mensen[i]}
                  key={classroom.ID}
                />
              )
            })}
          </Col>
          <Col className="menu-inner">
            <Row style={{ height: '100%', alignItems: 'flex-end' }}>
              <Col>
                <Button
                  className="bg-success w-100 mb-2 mt-1"
                  onClick={this.toOverview}
                >
                  Zelf selecteren
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    )
  }
}
