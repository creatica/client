import { List } from 'immutable'

export class Page<T> {
  index: number
  totalPages: number
  items: List<T>
}

export function Option<T>(value: T): Option<T> {
  if (!value) return { type: 'none' }
  return { type: 'some', value: value }
}

export type Option<T> =
  | {
      type: 'none'
    }
  | {
      type: 'some'
      value: T
    }

export type WithGetState<T> =
  | {
      type: 'loading'
    }
  | {
      type: 'loaded'
      data: Option<T>
    }
  | {
      type: 'error'
      reason: number
    }

export type WithPostState = 
  | {
    type: 'editing'
  }
  | {
    type: 'creating' | 'validating'
  }
  | {
    type: 'error'
    error?: string
  }
  | {
    type: 'success'
    message?: string
  }

export type WithPutState<T> =
  | {
    type: 'loading'
  }
  | {
    type: 'loaded' | 'editing'
    data: Option<T>
  }
  | {
    type: 'updating'
  }
  | {
    type: 'error'
    error?: string
  }
  | {
    type: 'success'
    message?: string
  }

export type WithDeleteState<T> = 
| {
  type: 'loading'
}
| {
  type: 'loaded' | 'editing'
  data: Option<T>
}
| {
  type: 'removing'
}
| {
  type: 'error'
  error?: string
}
| {
  type: 'success'
  message?: string
}

export class User {
  ID: string
  type: 'teacher' | 'student'
  name: string
  email: string
  password: string
  reservations: Reservation[]
}

export class Classroom {
  ID: string
  capacity: number
  peoplePresent: number
  building: string
  floor: string
  type: 'project' | 'stilte' | 'prive'
  reservations: Reservation[]
}

export class Reservation {
  dateTime: Date
  plannedDuration: number
  endDateTime: Date
  user: User
  classroom: Classroom
}
