import * as React from "react"
import { BrowserRouter, Route, Switch } from "react-router-dom"
import { LoginComponent } from "./components/Login";
import { RegisterComponent } from "./components/Register";
import { FilterComponent } from "./components/Filter";
import { SuggestionComponent } from "./components/Suggestion";
import { OverviewComponent } from "./components/Overview";
import { ReservationComponent } from "./components/Reservation";

export type ClientProps = {}

export const Client: React.SFC<ClientProps> = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route
          exact={true}
          path="/"
          render={() => {
            return (
              <LoginComponent />
            )
          }}
        />
        <Route
          path="/register"
          render={() => {
            return (
              <RegisterComponent />
            )
          }}
        />
        <Route
          exact={true}
          path="/filter"
          render={() => {
            return (
              <FilterComponent />
            )
          }}
        />
        <Route
          exact={true}
          path="/suggestions"
          render={() => {
            return (
              <SuggestionComponent />
            )
          }}
        />
        <Route
          exact={true}
          path="/overview"
          render={() => {
            return (
              <OverviewComponent />
            )
          }}
        />
        <Route
          exact={true}
          path="/reservation"
          render={() => {
            return (
              <ReservationComponent />
            )
          }}
        />
      </Switch>
    </BrowserRouter>
  )
}
