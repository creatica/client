# Roomy

De client voor de roomy app.
Gebruikt:

- React
- TypeScript
- Axios
- Immutable JS
- React Router DOM
- React State types voor elke REST / HTTP, CRUD methode
- Een aantal handige types voor Pagination
- Option type

## Installatie stappen

1. `git clone git@github.com:allondeveen/React-Template.git`
2. `yarn install`

## Starten

Om de applicatie te starten run je in je terminal of command line: `yarn start`. Als er een browservenster open is dan zal er automatisch een tabblad geopend worden waarin de applicatie draait. Als dat niet gebeurd dan kun je de applicatie [hier](http://localhost:3000) vinden.

Happy coding!
